# README #

### Folders Organization ###

### 3D Models ###
3D Models used in project

### Android Export APKs ###
APks ready to use and install in adroid mobile devices. The latest is always the more stable version.

### Unity Project Export ###
Unity Project exported after build to import in your unity dev 2019.3.4

### Others ###
Others, but important files to support the application repository

### What is this repository for? ###

### Resumo ###
As expectativas das instituições hospitares em fornecer um melhor atendimento médico com qualidade e segurança acompanham o progresso tecnológico que ocorre de forma exponencial. Deste modo para que os hospitais continuem aptos a fornecer um elevado grau de qualidade espera-se que estes não tenham incidentes de erros de identificação de seus pacientes internados. Na área da saúde podemos considerar este facto um erro básico que afeta a segurança e a qualidade do atendimento. Pode-se considerar ambos os fatores com igual ou maior importância em relação ao atendimento, já que estamos a lidar com a vida de pessoas. Leva contratempo que deixar mais grave a 
Em  uma solução direta para os problemas de erros de identificação essas intituições vem investindo notáveis valores no treinamento da suas equipas médicas para evitar tais incidentes que é um facto bom porém não suficiente. É nesse contexto que surge essa dissertação, onde se conceptualiza o desenvolvimento de uma aplicação android de realidade aumentada que realiza o reconhecimento facial dos pacientes internados, com o objetivo de obter com maior acurácia a identificação do paciente e suprimir a ocorrência de eventos adversos. Através de cálculos de análise de valor foi possível evidenciar a relevância da solução proposta e foi imperativo o uso de tecnologias promissoras que justifica e agrega um maior valor a solução.

Palavras-chave: Erro de Identificação, Reconhecimento Facial, Realidade Aumentada, Segurança, Eventos Adversos,  Aplicação Android.
 
### Abstract ###
The expectations of hospital institutions to provide better medical care with quality and safety follow the technological progress that occurs exponentially. Thus, for hospitals to remain able to provide a high degree of quality, it is expected that they are not having misidentification incidents with their inpatients. In the area of health, we can consider this fact a basic error that affects the safety and quality of care. Both factors can be considered with equal or greater importance in relation to medical care received, since we are dealing with people's lives.
In a direct solution to the problems of misidentification, these institutions have been investing remarkable values in training their medical teams to avoid such incidents, which is a good fact, but not enough. It is in this context that this dissertation arises, where the development of an augmented reality android application that performs facial recognition of inpatients is shown, in order to obtain more accurate identification of the patient and suppress the occurrence of adverse events. Through value analysis calculations it was possible to highlight the relevance of the proposed solution and it was imperative to use promising technologies that justifies and adds greater value to the solution.

Keywords: Identification Error, Facial Recognition, Augmented Reality, Security, Adverse Events, Android Application. 
